import React, { Component } from "react";
import Homepage from "./pages/Homepage";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import MoviePage from "./pages/MoviePage";
import AboutUs from "./pages/AboutUs";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/movie/:id" component={MoviePage} />
          <Route component={AboutUs} path="/aboutus"></Route>

          <Route component={Homepage} path="/"></Route>
        </Switch>
      </Router>
    );
  }
}
