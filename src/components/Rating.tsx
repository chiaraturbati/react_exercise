import React, { Component } from "react";
import styled from "styled-components";
import StarsComponent from "./StarsComponent";

interface Props {
  vote: number;
}

export default class RatingComponent extends Component<Props> {
  render() {
    const { vote } = this.props;
    return (
      <RatingWrapper>
        <h3>{vote}/5</h3>
        <StarsComponent vote={vote} />
      </RatingWrapper>
    );
  }
}

const RatingWrapper = styled.div``;
