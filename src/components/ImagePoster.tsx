import React, { Component } from "react";
import styled from "styled-components/macro";
import { Movie } from "../utils/types";
import { getImagePath } from "../utils/utils";

interface Props {
  movie: Movie;
}
export default class ImagePoster extends Component<Props> {
  render() {
    const { movie } = this.props;
    return (
      <>
        <ImgPoster src={getImagePath(movie)} />
      </>
    );
  }
}

const ImgPoster = styled.img`
  border-radius: 10px;
  padding: 5px;
  cursor: pointer;
  width: 150px;
  display: block;
  margin-left: 10px;
  margin-right: 10px;
`;
