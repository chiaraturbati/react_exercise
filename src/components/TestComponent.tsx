import React, { Component } from "react";
import styled from "styled-components";

interface MyType {
  stars: number;
  actors: string[];
}
interface Props {
  mytypesprops: MyType[];
}

export default class TestComponent extends Component<Props, MyType> {
  state: MyType = {
    stars: 0,
    actors: [],
  };
  componentDidMount() {
    this.setState({
      stars: 5,
      actors: ["Jon Doe", "Jane Doe"],
    });
  }

  render() {
    const { mytypesprops } = this.props;
    return (
      <>
        <div>
          {this.state.actors.map((actor) => (
            <h2> attori: {actor}</h2>
          ))}
          {mytypesprops.map((object: MyType) => (
            <p> {object.stars} </p>
          ))}
        </div>
      </>
    );
  }
}
