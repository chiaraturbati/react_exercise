import React, { Component } from "react";
import styled from "styled-components/macro";
import axios from "axios";
import { getImagePath } from "../utils/utils";
import Rating from "./Rating";
import { Movie, Genre, ResponseIMDB, ResponseGenre } from "../utils/types";
import ImagePoster from "./ImagePoster";
import { Link } from "react-router-dom";

interface Props {
  movies: Movie[];
}

export default class MovieList extends Component<Props> {
  render() {
    const { movies } = this.props;

    return (
      <>
        <ContainerMovieList>
          <Trending>Trending Movies</Trending>
          <RowMovies>
            {movies.map((movie, index) => (
              <Link to={`/movie/${movie.id}`}>
                <ContainerMovie key={index}>
                  <ImagePoster movie={movie} />
                </ContainerMovie>
              </Link>
            ))}
          </RowMovies>
        </ContainerMovieList>
      </>
    );
  }
}

const RowMovies = styled.div`
  margin: 0 20px;
  display: flex;
  position: relative;
  top: -90px;
  z-index: +50;
  overflow-x: scroll;
  overflow-y: hidden;
`;
const ContainerMovieList = styled.div`
  background-color: rgba(0, 0, 0, 2);
`;

const Trending = styled.h3`
  font-size: 24px;
  color: #f4f4f4;
  position: relative;
  top: -100px;
  z-index: +50;
  margin: 0 20px;
`;

const ContainerMovie = styled.div`
  object-fit: contain;
`;
