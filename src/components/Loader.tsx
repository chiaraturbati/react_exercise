import React, { Component } from "react";
import styled from "styled-components/macro";

export default class Loader extends Component {
  render() {
    return (
      <>
        <LoaderContainer>
          <LoadingText>Loading</LoadingText>
          <Loading></Loading>
        </LoaderContainer>
      </>
    );
  }
}
const Loading = styled.div`
  border: 16px solid #f3f3f3;
  border-top: 16px solid #3498db;
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
const LoaderContainer = styled.div`
  margin: 150px auto;
  width: 10%;
`;
const LoadingText = styled.h4`
  margin-left: 30px;
  margin-bottom: 20px;
`;
