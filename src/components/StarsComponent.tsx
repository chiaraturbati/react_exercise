import React, { Component } from "react";
import { FaStar } from "react-icons/fa";
import styled from "styled-components/macro";

interface Props {
  vote: number;
}

export default class StarsComponent extends Component<Props> {
  render() {
    const { vote } = this.props;

    return (
      <StarsContainer>
        <StarIcon stars={vote} /> <StarIcon stars={vote} /> 
        <StarIcon stars={vote} /> 
        <StarIcon stars={vote} />
        <StarIcon stars={vote} />
      </StarsContainer>
    );
  }
}
// interface StarProps {
//   stars: number;
// }
const StarIcon = styled(FaStar)<{ stars: number }>`
  color: white;
  &:nth-child(-n
      + ${(props) => {
        return props.stars;
      }}) {
    color: #ff6b00;
  }
`;

const StarsContainer = styled.div``;
