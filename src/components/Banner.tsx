import React, { Component } from "react";
import styled from "styled-components/macro";
import { getImagePath } from "../utils/utils";
import { Genre, Movie } from "../utils/types";

import StarsComponent from "./StarsComponent";

interface Props {
  movie: Movie;
  genres: Genre[];
  show: boolean;
}

export default class Banner extends Component<Props> {
  render() {
    const { movie, genres, show } = this.props;
    const vote = Math.round(movie.vote_average / 2);
    return (
      <div>
        <Header show={show} backgroundImage={getImagePath(movie)}>
          <Container show={show}>
            <Title>{movie.title}</Title>
            <StarsComponent vote={vote} />
            <TextGenre>
               <OverviewText className="primaryColor"> Overview:</OverviewText>
              Genre:
              {movie.genre_ids?.map((genre_id) => {
                return (
                  genres.find((genre) => {
                    return genre.id === genre_id;
                  })?.name + " "
                );
              })}
            </TextGenre>
            <Description>{movie.overview}</Description>
            {/* //Props returns back an object. In JavaScript, we
            can access to object elements with dot(.) notation. */}
          </Container>
        </Header>
      </div>
    );
  }
}

const Container = styled.div<{ show: boolean }>`
  margin: 50px;
  position: relative;
  top: ${(props) => (props.show ? "140px" : "200px")};
`;

const OverviewText = styled.p`
  font-size: 18px;
  font-weight: bold;
`;

const Header = styled.div<{ backgroundImage: string; show: boolean }>`
  height: 100vh;
  display: flex;
  align-items: center;

  background: linear-gradient(
      ${(props) =>
        props.show
          ? "0deg, rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0.1)"
          : "0deg, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.1)"}
    ),
    url(${(props) => props.backgroundImage});

  background-size: cover;
  background-repeat: no-repeat;
  position: relative;
`;
const Title = styled.p`
  margin-left: 15px;
  color: black;
  color: #f4f4f4;
  font-weight: bold;
  font-size: 40px;
`;

const TextGenre = styled.p`
  font-size: 16px;
  color: #f4f4f4;
`;
const Description = styled.p`
  font-size: 16px;
  color: #f4f4f4;
  max-width: 800px;
  font-weight: normal;
  word-wrap: break-word;
`;
const Vote = styled.p`
  font-size: 16px;
`;
