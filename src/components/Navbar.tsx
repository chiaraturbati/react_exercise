import React, { Component } from "react";
import styled from "styled-components/macro";
import { FaBell, FaUser, FaSearch } from "react-icons/fa";
import { Movie } from "../utils/types";
import { getImagePath } from "../utils/utils";
import { BrowserRouter as Router, Link } from "react-router-dom";

interface Props {
  movies: Movie[];
  show: boolean;
  toggleShow: () => void; // function: () => qualcosa è per dire che è una funzione, dovrò dire cosa ritorna, non c'è un valore di ritorno per questo il tipo restituito è void, se nella toggleShow dicevo anche return qualcosa, un array ad esempio, o un numero, qui gli avrei detto toggleShow: () => number[]; se come parametro avesse avuto ad esempio this.state.title che è una stringa l'avrei chiamata toggleShow: (val: string) => number[];
}

export default class Navbar extends Component<Props> {
  clickHandler = (movies: Movie[]) => {
    console.log("click");
    return movies.map((movie) => {
      return (
        <div className="row-movies">
          {movies.map((movie, index) => (
            <ContainerMovie key={index}>
              <ImgPoster src={getImagePath(movie)} />
            </ContainerMovie>
          ))}
        </div>
      );
    });
  };

  render() {
    return (
      <Nav>
        <NavbarContainer>
          <NavLogo>hellomovie </NavLogo>
          <LinkContainer>
            <SearchIcon size={20}> </SearchIcon>
            <NavLink>Home</NavLink>

            <NavLinkRoute to="/aboutus">About us</NavLinkRoute>

            <NavLink onClick={this.props.toggleShow}>Trending</NavLink>
            <UserIcon size={20}> </UserIcon>
            <BellIcon size={20}> </BellIcon>
          </LinkContainer>
        </NavbarContainer>
      </Nav>
    );
  }
}
const ContainerMovie = styled.div`
  /* display: inline; */
  object-fit: contain;
`;
const ImgPoster = styled.img`
  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 5px;
  width: 150px;
  display: block;
  margin-left: auto;
  margin-right: auto;
  /* width: 50%; */
`;
export const NavLink = styled.a`
  color: white;
  margin-top: 30px;
  width: 80px;
  height: 50px;
  font-size: 14px;
  transition: 0.2s ease-in-out;
  &:hover {
    transform: scale(1.05);
    cursor: pointer;
    transition: 0.2s ease-in-out;
  }
`;
export const NavLinkRoute = styled(Link)`
  color: white;
  margin-top: 30px;
  width: 80px;
  height: 50px;
  font-size: 14px;
  transition: 0.2s ease-in-out;
  text-decoration: none;
  &:hover {
    transform: scale(1.05);
    cursor: pointer;
    transition: 0.2s ease-in-out;
  }
`;

export const LinkContainer = styled.div`
  max-width: 400px;
  display: flex;
  justify-content: space-between;
`;
export const UserIcon = styled(FaUser)`
  margin-top: 30px;

  width: 80px;
  color: white;
  transition: 0.2s ease-in-out;
  &:hover {
    transform: scale(1.05);
    cursor: pointer;
    transition: 0.2s ease-in-out;
  }
`;
export const BellIcon = styled(FaBell)`
  margin-top: 30px;
  width: 80px;
  color: white;
  transition: 0.2s ease-in-out;
  &:hover {
    transform: scale(1.05);
    cursor: pointer;
    transition: 0.2s ease-in-out;
  }
`;
export const SearchIcon = styled(FaSearch)`
  margin-top: 30px;
  width: 80px;
  color: white;
  transition: 0.2s ease-in-out;
  &:hover {
    transform: scale(1.05);
    cursor: pointer;
    transition: 0.2s ease-in-out;
  }
`;
export const Nav = styled.nav`
  background: transparent;
  height: 80px;
  margin-top: -80px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
  position: sticky;
  top: 0;
  z-index: 10;
`;
export const NavbarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  height: 80px;
  z-index: 1;
  width: 100%;

  max-width: 1100px;
`;

export const NavLogo = styled.h1`
  color: #00c2ff;
  justify-self: flex-start;
  cursor: pointer;
  font-size: 38.83px;
  display: flex;
  align-items: center;

  font-weight: bold;
  text-decoration: none;
  text-transform: uppercase;
`;
