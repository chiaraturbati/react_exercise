import { Component } from "react";
import MovieList from "../components/MovieList";
import Banner from "../components/Banner";
import Navbar from "../components/Navbar";
import Loader from "../components/Loader";
import axios from "axios";
import "../App.css";
import { Genre, Movie, ResponseGenre, ResponseIMDB } from "../utils/types";

interface State {
  movies: Movie[];
  genres: Genre[];
  mytypes: MyType[];
  title: string;
  show: boolean;
}
interface MyType {
  stars: number;
  actors: string[];
}

export default class Homepage extends Component<{}, State> {
  state: State = {
    movies: [],
    genres: [],
    mytypes: [],
    title: "",
    show: false,
  };
  setTitle = () => {
    this.setState({ title: "Titoloneee" });
  };
  // {} è per dire che il "props è vuoto"
  // altrimenti avresti dovuto fare un interface Props {} e metterci <Props, State>

  componentDidMount() {
    this.asyncAwaitFunc();
    this.setTitle();
  }
  // Lo utilizzo per fare il numero minore possibile di chiamate

  //Async/Await
  asyncAwaitFunc = async () => {
    try {
      const popularMoviesRes = await axios.get<ResponseIMDB>(
        "https://api.themoviedb.org/3/movie/popular?api_key=d22df7a4c080fdc853176ebecce512ae&language=en-US&page=1"
      );
      const movieGenres = await axios.get<ResponseGenre>(
        "https://api.themoviedb.org/3/genre/movie/list?api_key=d22df7a4c080fdc853176ebecce512ae&language=en-US"
      );

      this.setState({
        movies: popularMoviesRes.data.results,
        genres: movieGenres.data.genres,
        mytypes: [
          { stars: 2, actors: ["Brad Pitt", "Angelina Jolie"] },
          { stars: 3, actors: ["Juno Temple", "Ellen Page"] },
          { stars: 5, actors: ["Mia Wasikowska", "Valeria Golino"] },
        ],
      });
    } catch (error) {}
  };

  toggleShow = () => {
    this.setState({ show: !this.state.show });
    // siccome la setState è collegata all'oggetto stato qui prende la modifica che lo stato deve avere, gli sto dicendo nell'oggetto che è lo stato devi modificare la proprietà show e assegnarla al valore this.state.show e voglio che sia l'opposto di quello che è già, per questo ci metto prima un punto esclamativo. ad esempio se avessi voluto cambiare anche title avrei aggiunto dopo this.state.show una virgola e poi title: "ciao"
  };
  render() {
    const { movies, genres, mytypes, title, show } = this.state;
    if (movies.length === 0) {
      return (
        <>
          <Navbar movies={movies} show={show} toggleShow={this.toggleShow} />
          <Loader />
        </>
      );
    }

    return (
      <>
        <Navbar movies={movies} show={show} toggleShow={this.toggleShow} />
        <Banner genres={genres} show={show} movie={movies[0]} />
        {show ? <MovieList movies={movies} /> : null}
      </>
    );
  }
}
