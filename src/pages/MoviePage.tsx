import React, { Component } from "react";
import { RouteComponentProps } from "react-router-dom";
import { Movie } from "../utils/types";
import axios from "axios";
import Banner from "../components/Banner";
import Loader from "../components/Loader";

interface Props extends RouteComponentProps<any> {
  id: number;
}
interface State {
  movie?: Movie;
}
export default class MoviePage extends Component<Props, State> {
  state: State = {
    movie: undefined,
  };
  componentDidMount() {
    this.asyncAwaitFunc();
  }
  asyncAwaitFunc = async () => {
    try {
      const singlemovie = await axios.get<Movie>(
        `https://api.themoviedb.org/3/movie/${this.props.match.params.id}?api_key=d22df7a4c080fdc853176ebecce512ae&language=en-US`
      );
      this.setState({
        movie: singlemovie.data,
      });
    } catch (error) {}
  };

  render() {
    const { id } = this.props;
    const { movie } = this.state;
    if (!movie) {
      return <Loader />;
    }

    return <Banner show={false} genres={[]} movie={movie}></Banner>;
    // <div>{this.props.match.params.id}</div>;
  }
}
