import { Component } from "react"
import { Movie } from "./types"
import { Genre } from "./types"

export const func1 = (counter: number) => {
    return counter + 1
}

export const getImagePath = (movie: Movie) => {
    return `https://www.themoviedb.org/t/p/w1280/${movie.poster_path}`
}
export const getVote = (movie: Movie) => {
    return `${movie.vote_average}`
}
export const getGenreId = (movie: Movie) => {
    return `${movie.genre_ids}`
}

